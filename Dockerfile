FROM ubuntu:14.04
MAINTAINER Andrew Long <aslong87@gmail.com>

# Get wget to install erlang source deb
RUN apt-get update && apt-get install -y \
  wget

# Change to a tmp directory, download and install the erlang-solutions sources
WORKDIR /tmp
RUN wget http://packages.erlang-solutions.com/erlang-solutions_1.0_all.deb
RUN dpkg -i erlang-solutions_1.0_all.deb

# Update apt after adding source and install erlang
RUN apt-get update && apt-get install -y \
  erlang \
  erlang-base-hipe

# Start erl shell by default
CMD ["erl"]
